<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\Copyright;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;

/**
 * The "ua_copyright_notice" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "ua_copyright_notice",
 *   type = "textfield",
 *   title = @Translation("Copyright Notice"),
 *   defaultValue = "The Arizona Board of Regents on behalf of <a href=""http://www.arizona.edu"" target=""_blank"">The University of Arizona</a>.",
 *   description = @Translation("A copyright notice for this site. The value here will appear after a ""Copyright YYYY"" notice (where YYYY is the current year)."),
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "copyright" = @Translation("Copyright"),
 *   },
 * )
 */
class CopyrightNotice extends SettingBase {}
