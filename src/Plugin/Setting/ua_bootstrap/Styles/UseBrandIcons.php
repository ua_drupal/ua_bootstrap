<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\Styles;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;

/**
 * The "ua_use_brand_icons" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "ua_use_brand_icons",
 *   type = "checkbox",
 *   weight = -1,
 *   title = @Translation("Use UA Brand Icons"),
 *   description = @Translation("The University of Arizona has created a set of custom icons for you to use."),
 *   defaultValue = 1,
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_styles" = @Translation("Styles"),
 *   },
 * )
 */
class UseBrandIcons extends SettingBase {}
