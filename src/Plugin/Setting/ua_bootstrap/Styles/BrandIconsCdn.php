<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\Styles;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\bootstrap\Utility\Element;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * The "ua_brand_icons_cdn" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "ua_brand_icons_cdn",
 *   type = "textfield",
 *   title = @Translation("URL to brand icons CDN"),
 *   defaultValue = "http://cdn.uadigital.arizona.edu/lib/ua-brand-icons/1.0.0/ua-brand-icons.css",
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_styles" = @Translation("Styles"),
 *   },
 * )
 */
class BrandIconsCdn extends SettingBase {

  /**
   * @inheritdoc
   */
   public function alterFormElement(Element $form, FormStateInterface $form_state, $form_id = NULL) {
     parent::alterFormElement($form, $form_state, $form_id);

     $setting_element = $this->getSettingElement($form, $form_state);
     $states = [
       'invisible' => [
         ':input[name="ua_use_brand_icons"]' => [ 'checked' => FALSE ],
       ],
     ];
     $setting_element->setProperty('states', $states);
   }

}
