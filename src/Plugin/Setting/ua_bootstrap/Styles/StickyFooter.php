<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\Styles;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;

/**
 * The "ua_sticky_footer" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "ua_sticky_footer",
 *   type = "checkbox",
 *   weight = -2,
 *   title = @Translation("Use Sticky Footer"),
 *   description = @Translation("This adds a technique to push the footer to the bottom of the page without having to set a footer height."),
 *   defaultValue = 1,
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_styles" = @Translation("Styles"),
 *   },
 * )
 */
class StickyFooter extends SettingBase {}
