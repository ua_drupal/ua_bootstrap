<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\FooterLogo;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * The "footer_logo_path" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "footer_logo_path",
 *   type = "textfield",
 *   title = @Translation("Path to custom footer logo"),
 *   description = @Translation("The path to the file you would like to use as your footer logo file instead of the logo in the header."),
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_footer_logo" = @Translation("Footer Logo"),
 *   },
 * )
 */
class FooterLogoPath extends SettingBase {

  /**
   * {@inheritdoc}
   */
  public static function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->isValueEmpty('footer_logo_path')) {
      $str_path = drupal_realpath($form_state->getValue('footer_logo_path'));
      if (!$str_path) {
        form_set_error('footer_logo_path', t('The custom logo path is invalid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    // If the path has been entered (either automatically or directly) check that
    // it exists. Only store it if does.
    if (!$form_state->isValueEmpty('footer_logo_path')) {
      $str_filename = drupal_realpath($form_state->getValue('footer_logo_path'));
      if ($str_filename === FALSE) {
        $form_state->setValue('footer_logo_path', '');
      }
      else {
        $form_state->setValue('footer_logo_path', $str_filename);
      }
    }
  }

}
