<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\FooterLogo;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;

/**
 * The "footer_logo_link" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "footer_logo_link",
 *   type = "textfield",
 *   weight = 10,
 *   title = @Translation("Footer logo link destination"),
 *   description = @Translation("Where should the footer logo link to. Example: &#x3C;front&#x3E;"),
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_footer_logo" = @Translation("Footer Logo"),
 *   },
 * )
 */
class FooterLogoLink extends SettingBase {}
