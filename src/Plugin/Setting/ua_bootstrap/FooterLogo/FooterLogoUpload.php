<?php
namespace Drupal\ua_bootstrap\Plugin\Setting\ua_bootstrap\FooterLogo;

use Drupal\bootstrap\Annotation\BootstrapSetting;
use Drupal\bootstrap\Plugin\Setting\SettingBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

$form['ua_footer_logo']['footer_logo_upload'] = array(
  '#type' => 'file',
  '#title' => t('Upload footer logo image'),
  '#maxlength' => 40,
  '#description' => t("If you don't have direct file access to the server, use this field to upload your footer logo."),
);

/**
 * The "footer_logo_upload" theme setting.
 *
 * @ingroup plugins_setting
 *
 * @BootstrapSetting(
 *   id = "footer_logo_upload",
 *   type = "file",
 *   title = @Translation("Upload footer logo image"),
 *   description = @Translation("If you don't have direct file access to the server, use this field to upload your footer logo."),
 *   groups = {
 *     "ua_bootstrap" = "UA Bootstrap Settings",
 *     "ua_footer_logo" = @Translation("Footer Logo"),
 *   },
 * )
 */
class FooterLogoUpload extends SettingBase {

  /**
   * {@inheritdoc}
   */
  public static function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the incoming file appropriately.
    $ary_validators = array('file_validate_is_image' => array(), 'file_validate_extensions' => array('png gif jpg jpeg'));
    $str_path = "";
    // Check for a new uploaded logo.
    $ary_file = file_save_upload('footer_logo_upload', $ary_validators);
    $obj_file = $ary_file[0];
    // $obj_file = file_save_upload('footer_logo_upload', $ary_validators, FALSE, 0);


    if (isset($obj_file)) {
      // File upload was attempted.
      if ($obj_file) {
        // Put the temporary file in form_values so we can save it on submit.
        $form_state->setValue('footer_logo_upload', $obj_file);
      }
      else {
        // File upload failed.
        form_set_error('footer_logo_upload', t('The footer logo could not be uploaded.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function submitForm(array &$form, FormStateInterface $form_state) {
    $str_filename = "";

    // If the user uploaded a new logo, save it to a permanent location.
    if (!$form_state->isValueEmpty('footer_logo_upload')) {
      $obj_file = $form_state->getValue('footer_logo_upload');
      $form_state->unsetValue('footer_logo_upload');
      $str_filename =  drupal_realpath(file_unmanaged_copy($obj_file->getFileUri(), NULL, FILE_EXISTS_REPLACE));
      $form_state->setValue('footer_logo_path', $str_filename);
    }
  }

}
