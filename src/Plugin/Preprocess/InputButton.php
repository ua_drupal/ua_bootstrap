<?php

/**
 * @file
 * Contains \Drupal\ua_bootstrap\Plugin\Preprocess\InputButton.
 */

namespace Drupal\ua_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Annotation\BootstrapPreprocess;
use Drupal\bootstrap\Utility\Element;
use Drupal\bootstrap\Utility\Variables;

/**
 * Pre-processes variables for the "input__button" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("input__button")
 */
class InputButton extends \Drupal\bootstrap\Plugin\Preprocess\InputButton {

  /**
   * {@inheritdoc}
   */
  public function preprocessElement(Element $element, Variables $variables) {
    if ($element->getProperty('value') == 'Search') {
      $icon = $element->getProperty('icon');
      $icon['#attributes']['class'] = array_map(function($value) {
        switch ($value) {
          case 'glyphicon':
            return 'ua-brand-icon';
          case 'glyphicon-search':
            return 'ua-brand-search';
          default:
            return $value;
        }
      }, $icon['#attributes']['class']);
      $element->setProperty('icon', $icon);
    }

    // Let bootstrap base theme preprocess the element normally now.
    parent::preprocessElement($element, $variables);
  }

}
